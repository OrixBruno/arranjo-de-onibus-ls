# Arranjo de Onibus Controle

Este projeto foi criado com o intuito de organizar passagens de onibus para eventos e aproveitando para testar algumas funcionalidades do Framework Angular Ionic.

## Instalação
Primeiro instale o pacote do [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html).
Segundo instale o pacote npm ionic e o cordova globalmente.
```bash
npm install -g ionic cordova
```
Terceiro instale o plataform tools do android para realizar o build and deploy da aplicação em seu dispositivo ou emulador [Plataform Tools](https://developer.android.com/studio/releases/platform-tools).

## Usabilidade
Primeiro instale os pacotes do npm rodando o comando abaixo.
```bash
npm install
```

## Declarações finais
Projeto com intuito de estudo da ferramenta ja aproveito uma necessidade pessoal.