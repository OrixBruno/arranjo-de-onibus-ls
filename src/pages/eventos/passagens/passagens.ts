import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Evento } from '../../../models/evento';
import { Passageiro } from '../../../models/passageiro';
import { PassageiroProvider } from '../../../providers/passageiro/passageiro';
import { FormBuilder, FormGroup, Validators } from '../../../../node_modules/@angular/forms';
import { Passagem } from '../../../models/passagem';
import { PassagemProvider } from '../../../providers/passagem/passagem';

@Component({
  selector: 'page-passagens',
  templateUrl: 'passagens.html',
})
export class PassagensPage {
  // evento selecionado
  evento: Evento;

  // lista de passageiros cadastrados
  passageiros: Array<Passageiro>;

  // lista de passagens ja adicionada
  passagens: Array<Passagem>;

  // formulario para adicionar passageiro
  formAdicionarPassageiro: FormGroup;

  constructor(private passagemProvider: PassagemProvider, public alertCtrl: AlertController, private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, private passageiroProvider: PassageiroProvider) {
    this.passageiros = new Array<Passageiro>();
    this.passagens = new Array<Passagem>();
    this.evento = this.navParams.get('evento');
    this.formAdicionarPassageiro = this.formBuilder.group({
      passageiro: ['', [Validators.required]],
      onibus: ['', [Validators.required]],
      poltrona: ['', [Validators.required]]
    });

    this.passageiros.push(new Passageiro(0, "Eduardo Bortes", "529999999"));
    this.passagens.push(new Passagem(1, 1, 1, this.passageiros[0], this.evento));
  }
  ionViewDidEnter() {
    this.passageiroProvider
      .getAll()
      .then(data => {
        if (data)
          this.passageiros = (<Passageiro[]>data);

      }, err => {

      });
    this.carregarPassagens();
  }
  carregarPassagens() {
    this.passagemProvider
      .getForEvent(this.evento)
      .subscribe(data => {
        if (data)
          this.passagens = data;
      }, err => {

      });
  }

  adicionarPassageiro() {
    if (this.formAdicionarPassageiro.invalid) {
      return;
    }
    this.passagemProvider.insert(new Passagem(
      null,
      this.formAdicionarPassageiro.value.poltrona,
      this.formAdicionarPassageiro.value.onibus,
      this.formAdicionarPassageiro.value.passageiro,
      this.evento,
    ))
      .subscribe(data => {
        if (data) {
          this.alertCtrl.create({
            title: "Passagem adicionada com sucesso.",
            message: `Ônibus: ${this.formAdicionarPassageiro.value.onibus} | Poltrona: ${this.formAdicionarPassageiro.value.poltrona}`,
            buttons: [{
              text: 'Ok',
            }]
          }).present();
          this.carregarPassagens();
        }
      }, err => {

      });
  }
  getListIndex(qtd) {
    return new Array(qtd || 0).fill(1);
  }
  retornaListaOnibus(qtdOnibus) {
    let listaQtdOnibus: Array<number> = [];
    let passageiroSelecionado = (<Passageiro>this.formAdicionarPassageiro.value.passageiro);

    for (let i = 0; i < qtdOnibus; i++) {
      listaQtdOnibus.push(i + 1);
    }
    if (passageiroSelecionado.id != null) {
      let listaPassagensFiltroIdPassageiro = this.passagens.filter(x => x.passageiro.id == passageiroSelecionado.id && );

      listaQtdOnibus.filter(x => x != )
    }

    return listaQtdOnibus.sort((a, b) => a - b);
  }
  filtrarListaPorOnibus(passagens: Array<Passagem>, onibus: number) {
    return passagens ? passagens.filter(x => x.numOnibus == onibus) : passagens;
  }
}
