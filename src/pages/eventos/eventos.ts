import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Evento } from '../../models/evento';
import { EventoProvider } from '../../providers/evento/evento';
import { AdicionarPage } from './adicionar/adicionar';
import { PassagensPage } from './passagens/passagens';

/**
 * Generated class for the EventosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html',
})
export class EventosPage {
  eventos: Array<Evento>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private eventoProvider: EventoProvider) {
  }
  ionViewDidEnter() {
    this.eventos = new Array<Evento>();
    this.eventos.push(new Evento(1, "congresso de distrito", "2018-12-12T00:00:00Z", 2, 46, "Mairiporã - SP"));
    this.eventoProvider
      .getAll()
      .subscribe(eventos => {
        this.eventos = eventos;
      }, error => {
        console.error(error);
      });
  }
  adicionarPassagem(evento: Evento) {
    this.navCtrl.push(PassagensPage, { evento: evento });
  }
  adicionarEvento() {
    this.navCtrl.push(AdicionarPage);
  }
}
