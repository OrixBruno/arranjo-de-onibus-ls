import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '../../../../node_modules/@angular/forms';
import { Evento } from '../../../models/evento';
import { EventoProvider } from '../../../providers/evento/evento';

/**
 * Generated class for the AdicionarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-adicionar',
  templateUrl: 'adicionar.html',
})
export class AdicionarPage {
  form: FormGroup;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, private eventoProvider: EventoProvider) {
    this.form = new FormGroup({
      nome: new FormControl("", [Validators.required]),
      data: new FormControl("", [Validators.required]),
      qtdOnibus: new FormControl("", [Validators.required]),
      qtdPoltronas: new FormControl("", [Validators.required]),
      local: new FormControl("", [Validators.required])
    });
  }
  adicionarEvento() {
    if (this.form.invalid) {
      return;
    }

    this.eventoProvider.insert(new Evento(
      null,
      this.form.value.nome,
      new Date(this.form.value.data).toISOString(),
      parseInt(this.form.value.qtdOnibus),
      parseInt(this.form.value.qtdPoltronas),
      this.form.value.local
    ))
      .subscribe(data => {
        if (data) {
          this.alertCtrl.create({
            title: "Evento adicionado",
            message: this.form.value.nome,
            buttons: [{
              text: 'Ok',
            }]
          }).present();
          this.navCtrl.pop();
        }
      }, err => {
        console.log(err);
      })
  }
}
