import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventosPage } from './eventos';
import { PassagensPage } from './passagens/passagens';
import { AdicionarPage } from './adicionar/adicionar';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    EventosPage,
    PassagensPage,
    AdicionarPage
  ],
  entryComponents: [
    PassagensPage,
    AdicionarPage
  ],
  imports: [
    PipesModule,
    DirectivesModule,
    IonicPageModule.forChild(EventosPage),
  ],
})
export class EventosPageModule { }
