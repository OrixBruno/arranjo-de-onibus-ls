import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassageirosPage } from './passageiros';
import { ListarPage } from './listar/listar';
import { AdicionarPage } from './adicionar/adicionar';
import { PipesModule } from '../../pipes/pipes.module';
import { PassageiroProvider } from '../../providers/passageiro/passageiro';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    PassageirosPage,
    AdicionarPage,
    ListarPage
  ],
  entryComponents: [
    PassageirosPage,
    AdicionarPage,
    ListarPage
  ],
  imports: [
    PipesModule,
    DirectivesModule,
    IonicPageModule.forChild(PassageirosPage),
  ],
  providers: [PassageiroProvider]
})
export class PassageirosPageModule { }
