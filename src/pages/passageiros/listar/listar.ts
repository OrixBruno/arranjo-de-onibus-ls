import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Passageiro } from '../../../models/passageiro';
import { PassageiroProvider } from '../../../providers/passageiro/passageiro';
import { AdicionarPage } from '../adicionar/adicionar';

/**
 * Generated class for the ListarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-listar',
  templateUrl: 'listar.html',
})
export class ListarPage {
  passageiros: Array<Passageiro>
  constructor(public navCtrl: NavController, public navParams: NavParams, private passageiroProvider: PassageiroProvider) {
  }
  ionViewDidEnter() {
    this.passageiros = new Array<Passageiro>();
    this.passageiroProvider
      .getAll()
      .then(result => {
        if (result)
          (<Passageiro[]>result).map(x => this.passageiros.push(x));
      });
  }
  ionViewDidLoad() {

  }

  viewPassenger(passageiro: Passageiro) {
    this.navCtrl.push(AdicionarPage, {
      passageiro: passageiro
    });
  }
}
