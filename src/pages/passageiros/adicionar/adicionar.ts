import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PassageiroProvider } from '../../../providers/passageiro/passageiro';
import { Passageiro } from '../../../models/passageiro';
import { Utils } from '../../../utils/utils';
import { RgValidator } from '../../../validators/rg.validator';

/**
 * Generated class for the AdicionarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-adicionar',
  templateUrl: 'adicionar.html',
})
export class AdicionarPage {
  passageiro: Passageiro;
  template: "Adicionar" | "Editar";
  form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private passageiroProvider: PassageiroProvider) {
    this.passageiro = navParams.get('passageiro');
    this.template = "Adicionar";

    this.form = new FormGroup({
      nome: new FormControl("", Validators.required),
      rg: new FormControl("", [Validators.required, RgValidator])
    });

    if (this.passageiro) {
      this.form.get('nome').setValue(this.passageiro.nome);
      this.form.get('rg').setValue(this.passageiro.rg);
      this.template = "Editar";
    }
  }
  submitForm() {
    if (this.template == 'Adicionar')
      this.cadastrarPassageiro()
    else
      this.editarPassageiro()
  }
  cadastrarPassageiro() {
    if (this.form.valid) {
      this.passageiroProvider
        .insert(new Passageiro(null, this.form.get('nome').value, Utils.retirarFormatacao(this.form.get('rg').value)))
        .then(data => {
          if (data) {
            let alert = this.alertCtrl.create({
              title: "Passageiro adicionado",
              message: "Nome: " + this.form.value.nome,
              buttons: [{
                text: 'Ok',
              }]
            });

            alert.present();
          };
        })
        .catch(err => {

          let alert = this.alertCtrl.create({
            title: "Error",
            message: err,
            buttons: [{
              text: 'Ok',
            }]
          });

          alert.present();
        });
    }
  }
  editarPassageiro() {
    if (this.form.valid) {
      this.passageiroProvider
        .update(new Passageiro(this.passageiro.id, this.form.get('nome').value, Utils.retirarFormatacao(String(this.form.get('rg').value))))
        .then(data => {
          if (data) {
            let alert = this.alertCtrl.create({
              title: "Passageiro adicionado",
              message: "Nome: " + this.form.value.nome,
              buttons: [{
                text: 'Ok',
              }]
            });

            alert.present();
          };
        })
        .catch(err => {

          let alert = this.alertCtrl.create({
            title: "Error",
            message: err,
            buttons: [{
              text: 'Ok',
            }]
          });

          alert.present();
        });
    }
  }
}
