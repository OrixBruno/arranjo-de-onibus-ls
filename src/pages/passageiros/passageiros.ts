import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ListarPage } from './listar/listar';
import { AdicionarPage } from './adicionar/adicionar';
/**
 * Generated class for the PassageirosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-passageiros',
  templateUrl: 'passageiros.html',
})
export class PassageirosPage {
  tabLista;
  tabAdicionar;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tabLista = ListarPage;
    this.tabAdicionar = AdicionarPage;
  }
  ionViewDidLoad() {
  }
}
