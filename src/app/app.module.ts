import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite'
import { HttpModule } from "@angular/http";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PassageirosPageModule } from '../pages/passageiros/passageiros.module';
import { DatabaseProvider } from '../providers/database/database';
import { PassageiroProvider } from '../providers/passageiro/passageiro';
import { EventosPageModule } from '../pages/eventos/eventos.module';

//Registrando PTBR
import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { File } from '@ionic-native/file';
import { EventoProvider } from '../providers/evento/evento';
import { PassagemProvider } from '../providers/passagem/passagem';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/forkJoin';

registerLocaleData(localePtBr);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    PassageirosPageModule,
    HttpModule,
    EventosPageModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    File,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    PassageiroProvider,
    EventoProvider,
    PassagemProvider
  ]
})
export class AppModule {}
