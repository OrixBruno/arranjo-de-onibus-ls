import { DatabaseProvider } from "../database/database";
import { SQLiteObject } from "../../../node_modules/@ionic-native/sqlite";
import { ReplaySubject, Observable } from "../../../node_modules/rxjs";

export class Base {
    constructor(public dbProvider: DatabaseProvider) { }
    insertCmmd(command: string, data: any[]): Observable<any> {
        let $subject = new ReplaySubject<any>();
        this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                db.executeSql(command, data)
                    .then(data => { $subject.next(true); $subject.complete(); })
                    .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });
            })
            .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });
        return $subject;
    }
}