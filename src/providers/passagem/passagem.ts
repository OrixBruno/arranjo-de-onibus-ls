import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { Passagem } from '../../models/passagem';
import { ReplaySubject, Observable } from '../../../node_modules/rxjs';
import { SQLiteObject } from '../../../node_modules/@ionic-native/sqlite';
import { Passageiro } from '../../models/passageiro';
import { Evento } from '../../models/evento';
import { Base } from '../base/base';

@Injectable()
export class PassagemProvider extends Base {
  constructor(public dbProvider: DatabaseProvider) {
    super(dbProvider);
  }

  public getAll(): Observable<Array<Passagem>> {
    let $subject = new ReplaySubject<Array<Passagem>>();

    this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let command = 'SELECT * FROM evento';

        db.executeSql(command, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let eventos: Array<Passagem> = new Array<Passagem>();
              for (let i = 0; i < data.rows.length; i++) {
                let item = data.rows.item(i);
                let evento = new Passagem(item.id, item.poltrona, item.numOnibus, null, null);
                eventos.push(evento);
              }
              $subject.next(eventos);
            } else {
              $subject.next(new Array<Passagem>());
            }
            $subject.complete();
          })
          .catch((e) => { $subject.error(e); $subject.complete(); });
      })
      .catch((e) => { $subject.error(e); $subject.complete(); });

    return $subject;
  }
  public getForEvent(evento: Evento) {
    let $subject = new ReplaySubject<Array<Passagem>>();
    let command = "SELECT a.id, a.poltrona, a.numOnibus, a.evento, a.passageiro, b.nome, b.rg FROM Passagem as a INNER JOIN Passageiro as b WHERE a.evento = ?"

    this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        db.executeSql(command, [evento.id])
          .then((data: any) => {
            let passagens: Array<Passagem> = new Array<Passagem>();

            if (data.rows.length > 0) {
              for (let i = 0; i < data.rows.length; i++) {
                let item = data.rows.item(i);
                let passagem = new Passagem(item.id, item.poltrona, item.numOnibus, new Passageiro(item.passageiro, item.nome, item.rg), evento);
                passagens.push(passagem);
              }
            }

            $subject.next(passagens);
            $subject.complete();
          })
          .catch((e) => { $subject.error(e); $subject.complete(); });
      })
      .catch((e) => { $subject.error(e); $subject.complete(); });
    return $subject;
  }
  public insert(passagem: Passagem): Observable<boolean> {
    let command = "INSERT INTO Passagem (passageiro, evento, poltrona, numOnibus) VALUES (?, ?, ?, ?)";
    let data = [passagem.passageiro.id, passagem.evento.id, passagem.poltrona, passagem.numOnibus];

    return this.insertCmmd(command, data);
    
    // this.dbProvider.getDB()
    //   .then((db: SQLiteObject) => {
    //     db.executeSql(command, data)
    //       .then(data => { $subject.next(true); $subject.complete(); })
    //       .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });
    //   })
    //   .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });
  }
}
