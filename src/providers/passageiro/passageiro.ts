import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { Passageiro } from '../../models/passageiro';
import { SQLiteObject } from '@ionic-native/sqlite';

/*
  Generated class for the PassageiroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PassageiroProvider {

  constructor(private dbProvider: DatabaseProvider) { }

  public insert(passageiro: Passageiro) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO passageiro (nome, rg) values (?, ?)';
        let data = [passageiro.nome, passageiro.rg];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public update(passageiro: Passageiro) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'UPDATE passageiro SET nome = ?, rg = ? where id = ?';
        let data = [passageiro.nome, passageiro.rg, passageiro.id];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public remove(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'DELETE FROM passageiro where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM passageiro where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);
              let passageiro = new Passageiro(item.id, item.nome, item.rg);
              return passageiro;
            }

            return null;
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM passageiro';

        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let passageiros: Array<Passageiro> = new Array<Passageiro>();
              for (var i = 0; i < data.rows.length; i++) {
                let item = data.rows.item(i);
                var passageiro = new Passageiro(item.id, item.nome, item.rg);
                passageiros.push(passageiro);
              }
              return passageiros;
            } else {
              return [];
            }
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

}
