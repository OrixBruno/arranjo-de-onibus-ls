import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Evento } from '../../models/evento';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs';

/*
  Generated class for the EventoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventoProvider {

  constructor(private dbProvider: DatabaseProvider) { }
  public getAll(): Observable<Array<Evento>> {
    let $subject = new ReplaySubject<Array<Evento>>();

    this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let command = 'SELECT * FROM evento';

        db.executeSql(command, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let eventos: Array<Evento> = new Array<Evento>();
              for (let i = 0; i < data.rows.length; i++) {
                let item = data.rows.item(i);
                let evento = new Evento(item.id, item.nome, item.data, item.qtdOnibus, item.qtdPoltronas, item.local);
                eventos.push(evento);
              }
              $subject.next(eventos);
            } else {
              $subject.next(new Array<Evento>());
            }
            $subject.complete();
          })
          .catch((e) => { $subject.error(e); $subject.complete(); });
      })
      .catch((e) => { $subject.error(e); $subject.complete(); });

    return $subject;
  }
  public insert(evento: Evento): Observable<boolean> {
    let $subject = new ReplaySubject<boolean>();

    this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let command = 'INSERT INTO evento (nome, data, qtdOnibus, qtdPoltronas, local) VALUES (?, ?, ?, ?, ?)';
        let data = [evento.nome, evento.data, evento.qtdOnibus, evento.qtdPoltronas, evento.local];

        db.executeSql(command, data)
          .then(data => { $subject.next(true); $subject.complete(); })
          .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });
      })
      .catch((e) => { console.error(e); $subject.next(false); $subject.complete(); });

    return $subject;
  }
}

