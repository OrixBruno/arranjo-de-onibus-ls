import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Http } from '@angular/http';
import { File } from '@ionic-native/file';
import { Observable } from 'rxjs/Observable';
import { Evento } from '../../models/evento';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
  constructor(private http: Http, private sqlite: SQLite, private file: File) {
    // file.listDir(this.file.applicationDirectory, '/www/assets/sql')
    //   .then(dir => {
    //     console.log(dir);
    //   })
  }
  public getDB() {
    return this.sqlite.create({
      name: 'passagens.db',
      location: 'default'
    });
  }
  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        // Criando as tabelas
        this.createTables(db);
      })
      .catch(e => console.log(e));
  }
  private createTables(db: SQLiteObject) {
    Observable.forkJoin([
      this.http.get('../../assets/sql/create_evento.sql').map(res => res.text()),
      this.http.get('../../assets/sql/create_passageiro.sql').map(res => res.text()),
      this.http.get('../../assets/sql/create_passagem.sql').map(res => res.text())
    ])
      .subscribe(([eventoSql, passageiroSql, passagemSql]) => {
        db.sqlBatch([
          [eventoSql],
          [passageiroSql],
          [passagemSql]
        ])
          .then(() => {
            console.log("create tables success!");
            // this.insertDefaultItems(db);
          })
          .catch(e => console.error('create tables failed.', e));
      })
  }

  private insertDefaultItems(db: SQLiteObject) {
    db.sqlBatch([
      ['INSERT INTO passageiro (nome, rg) values (?, ?)', ['Bruno Silva do Nascimento', '521076134']]
    ])
      .then(() => console.log('Dados de passageiro padrões incluídos'))
      .catch(e => console.error('Erro ao incluir passeiro padrões', e));


    db.sqlBatch([
      ['INSERT INTO evento (nome, data, qtdOnibus, qtdPoltronas, local) values (?, ?, ?, ?, ?)', ['Congresso - 2018', "2018-11-12T12:12:04.940Z", 1, 48, "Mairiporã - SP"]]
    ])
      .then(() => console.log('Dados de evento padrões incluídos'))
      .catch(e => console.error('Erro ao incluir evento padrões', e));
  }
}
