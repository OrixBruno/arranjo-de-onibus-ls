import { NgModule } from '@angular/core';
import { AdicionarComponent } from './adicionar/adicionar';
@NgModule({
	declarations: [AdicionarComponent],
	imports: [],
	exports: [AdicionarComponent]
})
export class ComponentsModule {}
