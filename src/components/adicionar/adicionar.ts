import { Component } from '@angular/core';

/**
 * Generated class for the AdicionarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'adicionar',
  templateUrl: 'adicionar.html'
})
export class AdicionarComponent {

  text: string;

  constructor() {
    console.log('Hello AdicionarComponent Component');
    this.text = 'Hello World';
  }

}
