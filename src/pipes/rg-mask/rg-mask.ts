import { Pipe, PipeTransform } from '@angular/core';
import { Utils } from '../../utils/utils';

/**
 * Generated class for the RgMaskPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'rgMask',
})
export class RgMaskPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return Utils.mascaraRg(value);
  }
}
