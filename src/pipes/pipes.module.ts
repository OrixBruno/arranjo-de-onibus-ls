import { NgModule } from '@angular/core';
import { RgMaskPipe } from './rg-mask/rg-mask';
@NgModule({
	declarations: [RgMaskPipe],
	imports: [],
	exports: [RgMaskPipe],
	providers: [RgMaskPipe]
})
export class PipesModule {}
