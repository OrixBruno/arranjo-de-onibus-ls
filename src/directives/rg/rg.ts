import { Directive, ElementRef, Optional, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Utils } from '../../utils/utils';

/**
 * Generated class for the RgDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[rgMask]' // Attribute selector
})
export class RgDirective {
  private element: HTMLInputElement;

  constructor(private elementRef: ElementRef, @Optional() private control: NgControl) {
  }
  ngAfterViewInit() {
    this.element = this.elementRef.nativeElement.querySelector('input');
  }
  @HostListener('keyup', ['$event'])
  // aplica a diretiva
  apply(event: KeyboardEvent) {
    // recupera o valor do elemento - removendo a formatacao
    let unformatted = Utils.retirarFormatacao(this.element.value).substr(0, 9);

    // formata o valor
    let formatted = Utils.mascaraRg(unformatted);

    // aplica o valor formatado
    this.element.value = formatted;
    this.control && this.control.control.setValue(formatted);
  }
}
