import { NgModule } from '@angular/core';
import { RgDirective } from './rg/rg';
@NgModule({
	declarations: [RgDirective],
	imports: [],
	exports: [RgDirective],
	providers: [RgDirective]
})
export class DirectivesModule {}
