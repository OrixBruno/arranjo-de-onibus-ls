export class Evento {
    constructor(
        public id: number,
        public nome: string,
        public data: string,
        public qtdOnibus: number,
        public qtdPoltronas: number,
        public local: string
    ) {
    }
}