import { Passageiro } from "./passageiro";

export class Onibus {
    qtdMaxPassageiros: number;
    passageiros: Array<Passageiro>;

    constructor(qtdMaxPassageiros: number, passageiros?: Array<Passageiro>) {
        this.qtdMaxPassageiros = qtdMaxPassageiros;
        this.passageiros = passageiros;
    }
}