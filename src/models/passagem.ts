import { Evento } from "./evento";
import { Passageiro } from "./passageiro";

export class Passagem {
    constructor(public id: number, public poltrona: number, public numOnibus: number, public passageiro?: Passageiro, public evento?: Evento) { }
}