CREATE TABLE IF NOT EXISTS `Passagem` (
`id` INTEGER PRIMARY KEY AUTOINCREMENT,
`passageiro` INTEGER NULL,
`evento` INTEGER NULL,
`poltrona` INTEGER NOT NULL,
`numOnibus` INTEGER NOT NULL,
CONSTRAINT `fk_evento_id`
  FOREIGN KEY (`evento`)
  REFERENCES `Evento` (`id`),
CONSTRAINT `fk_passeiro_id`
  FOREIGN KEY (`passageiro`)
  REFERENCES `Passageiro` (`id`))