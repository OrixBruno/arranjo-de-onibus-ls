import { AbstractControl } from '@angular/forms';
import { Utils } from '../utils/utils';

export function RgValidator(control: AbstractControl) {
    if (control.touched && Utils.retirarFormatacao(control.value).length < 8) {
        return { rgValid: false };
    }
    return null;
}