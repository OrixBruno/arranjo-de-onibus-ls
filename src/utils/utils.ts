export class Utils {
    static retirarFormatacao(valor: string) {
        return valor.replace(/(\.|\/|\-)/g, "");
    }
    static mascaraRg(valor: string) {
        let v = valor.replace(/\D/g, "");
        return v.replace(/(\d{1,2})(\d{3})(\d{3})(\d{1})$/, "$1.$2.$3-$4");
    }
    static mascaraCpf(valor: string) {
        return valor.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
    }
}